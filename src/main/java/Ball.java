import java.awt.*;
import java.util.Random;

public class Ball extends Rectangle{

    private Random random;
    private int xVelocity;
    private int yVelocity;
    int speed=2;

    public int getxVelocity() {
        return xVelocity;
    }

    public void setxVelocity(int xVelocity) {
        this.xVelocity = xVelocity;
    }

    public int getyVelocity() {
        return yVelocity;
    }

    public void setyVelocity(int yVelocity) {
        this.yVelocity = yVelocity;
    }

    public Ball(int x, int y, int width, int height) {
        super(x,y,width,height);
        random = new Random();
        int randomXDirection=random.nextInt(2);
        if(randomXDirection==0){
            randomXDirection--;
        }
        setXDirection(randomXDirection*speed);

        int randomYDirection=random.nextInt(2);
        if(randomYDirection==0){
            randomYDirection--;
        }
        setYDirection(randomYDirection*speed);
    }

    public void setXDirection(int randomXDirection){
        xVelocity = randomXDirection;
    }

    public void setYDirection(int randomYDirection){
        yVelocity=randomYDirection;
    }

    public void move(){
        x=x+xVelocity;
        y=y+yVelocity;
    }

    public void draw(Graphics g){
        g.setColor(Color.white);
        g.fillOval(x,y,height,width);
    }
}
