import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;

public class GamePanel extends JPanel implements Runnable {

    private static final int GAME_WIDTH=500;
    private static final int GAME_HEIGHT=500;
    private static final Dimension SCREEN_SIZE = new Dimension(GAME_WIDTH,GAME_HEIGHT);
    private static final int BALL_DIAMETER=20;
    private static final int PADDLE_WIDTH= 80;
    private static final int PADDLE_HEIGHT=10;
    private static final int BRICK_WIDTH=30;
    private static final int BRICK_HEIGHT=30;
    private Thread gameThread;
    private Image image;
    private Graphics graphics;
    private Random random;
    private Paddle paddle;
    private Ball ball;
    private FourBricksSquare bricks;

    public GamePanel() {

        newPaddle();
        newBall();
        newFourBricksSquare();
        this.setFocusable(true);
        this.addKeyListener(new AL());
        this.setPreferredSize(SCREEN_SIZE);

        gameThread = new Thread(this);
        gameThread.start();

    }

    public void newBall(){
        ball =new Ball((GAME_WIDTH/2)-(BALL_DIAMETER/2),300,BALL_DIAMETER,BALL_DIAMETER);
    }

    public void newPaddle(){
        paddle = new Paddle((GAME_WIDTH/2)-(PADDLE_WIDTH/2),GAME_HEIGHT-PADDLE_HEIGHT,PADDLE_WIDTH,PADDLE_HEIGHT);
    }

    public void newFourBricksSquare(){
        bricks=new FourBricksSquare(GAME_HEIGHT/2,150,BRICK_WIDTH,BRICK_HEIGHT);
    }

    public void paint(Graphics g){

        image = createImage(getWidth(),getHeight());
        graphics = image.getGraphics();
        draw(graphics);
        g.drawImage(image,0,0,this);

    }

    public void draw(Graphics g){
        paddle.draw(g);
        ball.draw(g);
        bricks.draw(g);
    }

    public void move(){
        paddle.move();
        ball.move();
    }

    public void checkCollision(){
        for(int i=0 ; i<bricks.bricksList().size(); i++){
            if(ball.intersects(bricks.bricksList().get(i))){
                ball.setXDirection(ball.getxVelocity()*(-1));
                ball.setYDirection(ball.getyVelocity()*(-1));
            }
        }

        if(ball.intersects(paddle)){
            ball.setxVelocity(Math.abs(ball.getxVelocity()));
            ball.setxVelocity(ball.getxVelocity()+1);
            if(ball.getyVelocity()>0){
                ball.setyVelocity(ball.getyVelocity()+1);
            }
            else {
                ball.setyVelocity(ball.getyVelocity()-1);
            }
            ball.setXDirection(ball.getxVelocity()*(-1));
            ball.setYDirection(ball.getyVelocity()*(-1));
        }


        if(paddle.x<=0){
            paddle.x=0;
        }
        if(paddle.x>=(GAME_WIDTH-PADDLE_WIDTH)){
            paddle.x=GAME_WIDTH-PADDLE_WIDTH;
        }


        if(ball.y<=0){
            ball.setYDirection(-ball.getyVelocity());
        }
        if(ball.x<=0){
            ball.setXDirection(-ball.getxVelocity());
        }
        if(ball.x>=(GAME_WIDTH-BALL_DIAMETER)){
            ball.setXDirection(-ball.getxVelocity());
        }
    }

    public void run(){

        long lastTime = System.nanoTime();
        double amountOfTicks=60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta=0;
        while(true){
            long now = System.nanoTime();
            delta +=(now-lastTime)/ns;
            lastTime=now;
            if(delta>=1){
                move();
                checkCollision();
                repaint();
                delta--;
            }
        }

    }

    public class AL extends KeyAdapter{
        public void keyPressed(KeyEvent e){
            paddle.keyPressed(e);
        }

        public void keyReleased(KeyEvent e){
            paddle.keyReleased(e);
        }
    }
}
