import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class FourBricksSquare  {

    private int xCenter;
    private int yCenter;

    private Brick brick1;
    private Brick brick2;
    private Brick brick3;
    private Brick brick4;


    public FourBricksSquare(int xCenter, int yCenter,int BRICK_WIDTH, int BRICK_HEIGHT) {

        this.xCenter = xCenter;
        this.yCenter = yCenter;
        brick1=new Brick(xCenter-BRICK_WIDTH-2,yCenter-BRICK_HEIGHT-2,BRICK_WIDTH,BRICK_HEIGHT);
        brick2=new Brick(xCenter+2,yCenter-BRICK_HEIGHT-2,BRICK_WIDTH,BRICK_HEIGHT);
        brick3=new Brick(xCenter-BRICK_WIDTH-2,yCenter+2,BRICK_WIDTH,BRICK_HEIGHT);
        brick4=new Brick(xCenter+2,yCenter+2,BRICK_WIDTH,BRICK_HEIGHT);

    }

    public List<Brick> bricksList(){
        return new ArrayList<Brick>(List.of(brick1,brick2,brick3,brick4));
    }

    public void draw(Graphics g){

        brick1.draw(g);
        brick2.draw(g);
        brick3.draw(g);
        brick4.draw(g);

    }

    public void removeBrick(){
    }

    public void checkBrickCollision(Ball ball,Graphics g,int BRICK_WIDTH,int BRICK_HEIGHT){
        for(int i=0;i<bricksList().size();i++){
            if(bricksList().get(i).intersects(ball)){
                bricksList().get(i).erase(g,BRICK_WIDTH,BRICK_HEIGHT);
            }
        }
    }

}
